# LoraLink v1.0
LoraLink is a web-based application that specializes in providing API services exclusively for the Chirpstack platform, which is primarily utilized for LoRaWAN communication. This system operates independently of Chirpstack's communication infrastructure and instead obtains data directly from the Influx database, where Chirpstack data is stored. LoraLink offers diverse functionalities for constructing APIs from various data sources, including databases, and features an interactive admin interface. Administrators can effortlessly manage data sources, APIs, users, and tokens using this interface.

![image1](Images/home.png)
![image1](Images/api_build_page.png)

# Key Features
  
  - The application allows the administrator to perform the following actions:
    - Datasource management
    - API management
    - User management
    - Token management
  - With their tokens, users can gain access to the API. 

# Getting Started

- To utilize the system, follow these steps:
    - Access the system via its domain name.
    - Log in to the admin interface.
    - Navigate to the Datasources tab and add a data source containing Chirpstack data.
    - Create an API by visiting the API Manage tab.
    - Proceed to the API Build tab, choose the desired API, and click on "Build." Fill in the required fields and click "Create." You will now receive an API link that you can share with others. However, to access this API, users must obtain authorization by following these steps:
    - Go to the Users tab and generate a new user.
    - Add permissions for the user to the corresponding APIs in the Permissions tab.
    - Generate tokens for users in the Tokens tab and share them, along with the API, with the relevant users.
    - In the Settings tab, configure the server's base URL or domain name.

# Type of API endpoints
  - Data with a predefined limit.
    - http://127.0.0.1:8000/api/v1/api-name
  - Time frame-based data.
    - http://127.0.0.1:8000/api/v1/api-name/filter/minutes/10
    - http://127.0.0.1:8000/api/v1/api-name/filter/hours/10
    - http://127.0.0.1:8000/api/v1/api-name/filter/days/10
    - http://127.0.0.1:8000/api/v1/api-name/filter/weeks/10
    - http://127.0.0.1:8000/api/v1/api-name/filter/months/20
    - http://127.0.0.1:8000/api/v1/api-name/filter/years/10
  - Time range-based data.
    - http://127.0.0.1:8000/api/v1/api-name/from/2022-04-27T06:05:27Z/to/2023-03-27T06:05:27Z

# Access from command line
  - curl -X GET http://127.0.0.1:8000/api/v1/awsktkd -H 'Authorization: Token b85b7ea45b7954db53ccb2f3396b4650236dcdd3'
  - curl -X GET http://127.0.0.1:8000/api/v1/awsktkd/filter/minutes/10 -H 'Authorization: Token b85b7ea45b7954db53ccb2f3396b4650236dcdd3'
  - curl -X GET http://127.0.0.1:8000/api/v1/awsktkd/filter/hours/10 -H 'Authorization: Token b85b7ea45b7954db53ccb2f3396b4650236dcdd3'
  - curl -X GET http://127.0.0.1:8000/api/v1/awsktkd/filter/days/10 -H 'Authorization: Token b85b7ea45b7954db53ccb2f3396b4650236dcdd3'
  - curl -X GET http://127.0.0.1:8000/api/v1/awsktkd/filter/weeks/10 -H 'Authorization: Token b85b7ea45b7954db53ccb2f3396b4650236dcdd3'
  - curl -X GET http://127.0.0.1:8000/api/v1/awsktkd/filter/months/20 -H 'Authorization: Token b85b7ea45b7954db53ccb2f3396b4650236dcdd3'
  - curl -X GET http://127.0.0.1:8000/api/v1/awsktkd/filter/years/10 -H 'Authorization: Token b85b7ea45b7954db53ccb2f3396b4650236dcdd3'
  - curl -X GET http://127.0.0.1:8000/api/v1/awsktkd/from/2022-04-27T06:05:27Z/to/2023-03-27T06:05:27Z -H 'Authorization: Token b85b7ea45b7954db53ccb2f3396b4650236dcdd3'

# Prerequisites
  - Python 3
  - Influxdb python library
  - Django
  - Django REST framework
  - MariaDB

# Local installation setup
  - Install dependencies
    - sudo apt-get install libmysqlclient-dev
    - sudo apt install mariadb-server mariadb-client
    - sudo mysql_secure_installation
  - Install python libraries
    - pip install Django
    - pip install djangorestframework
    - pip install mysqlclient
    - pip install influxdb
  - Create database
    - mysql -u root -p
    - create database lora_api_service;
    - exit;
    - Note : (If mysql authentication denied )
        - sudo mysql
        - ALTER USER root@localhost IDENTIFIED  BY "password";
        - exit;
  - Go to the project root directory
    - python3 manage.py makemigrations
    - python3 manage.py migrate
    - python3 manage.py createsuperuser
    - python manage.py runserver
    

# Contributing

# License

# Troubleshooting

# Acknowledgments

# Contribute to development

# Changelog
